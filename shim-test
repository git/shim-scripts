#!/bin/bash

# set -x

# Do all the steps needed to test a set of signed shim binaries for a
# given hash, distribution and architecture.

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
GIT_DIR=shim@jack.einval.org:build/shim.git
ARTIFACTS=shim@jack.einval.org:artifacts
HOSTNAME=$(hostname --fqdn)
MAILTO=steve@einval.com
BOOT_TIMEOUT=120
LOG=""
SUMMARY=""
TESTS_DIR=${SCRIPT_DIR}/tests
DT=${SCRIPT_DIR}/describe-test
SSH_OPTS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

usage () {
    cat <<EOF
$0 <options> - test a shim build

options:

 -a ARCH1[,ARCH2,...]   build is for the specified architecture(s)
 -d DIST1[,DIST2,...]   build is for the specified Debian distribution(s)
 -h HASH                the git hash of the build
 -t TEST   		run the specified test only
 -n        		dry-run, just lists tests

EOF
}

check_error () {
    if [ $1 -ne 0 ]; then
	echo "$0 $ARTSIN $HASH $DIST $ARCH failed with error $1: $2"
	echo "$0 $ARTSIN $HASH $DIST $ARCH failed with error $1: $2" | \
            mail -s "testing failed on $HOSTNAME" $MAILTO
	exit 1
    fi
}

arch_to_efi () {
    local ARCH=$1
    case $ARCH in
	amd64)
            EFI=x64;;
	i386)
            EFI=ia32;;
	arm64)
            EFI=aa64;;
    esac
    echo "$EFI"
}

run_vm_command () {
    local VM_CONF=$1
    local CMD="$2"

    . $PWD/$VM_CONF

    log "  Running command: \"$CMD\""
    OUTPUT=$(ssh ${SSH_OPTS} ${VM_IP} -p${SSH_PORT} -lroot -- "$CMD" 2>&1 >> $LOGFILE)
    error=$?
    log "  command finished with error $error"
    return $error
}

shutdown_vm () {
    local VM_CONF=$1
    local METHOD=$2
    local EXTRA_CMDS=$3

    . $PWD/$VM_CONF

    if [ -r $PIDFILE ]; then
	PID=$(cat $PIDFILE)
    else
	echo "$0: Can't read pidfile $PIDFILE"
	return
    fi

    error=0
    if [ "$METHOD" = "ssh" ]; then
	if [ "$EXTRA_CMDS"x = "UPDATE-UPGRADE"x ]; then
	    log "Running update/upgrade in VM"
	    ssh ${SSH_OPTS} ${VM_IP} -p${SSH_PORT} -lroot \
		"apt-get autoclean && apt-get update -y && apt-get -o Dpkg::Options::=--force-confnew -o Dpkg::Options::=--force-confmiss dist-upgrade -y --autoremove --purge" >> $LOGFILE
	    error=$?
	    log "  update/upgrade finished with error $error"
	fi

	MAX_WAIT=30 # how long to wait for clean shutdown
	ssh ${SSH_OPTS} ${VM_IP} -p${SSH_PORT} -lroot poweroff
	TRY=0
	while [ $TRY -lt $MAX_WAIT ]; do
	    if [ -r $PIDFILE ] && (ps $PID >/dev/null); then
		# echo "$0: kvm / qemu still running as pid $PID after $TRY seconds"
		sleep 1
		TRY=$(($TRY + 1))
	    else
		break
	    fi
	done	
    fi

    if [ -r $PIDFILE ] && (ps $PID >/dev/null); then
	echo "$0: killing pid $PID"
	kill -9 $PID
    fi
    rm -f $VM_CONF $PIDFILE
    return $error
}    

log () {
    echo "$*"
    echo "$*" >> $LOGFILE
}

summary () {
    echo "$*"
    echo "$*" >> $SUMMARY
}

log_and_summary () {
    echo "$*"
    echo "$*" >> $LOGFILE
    echo "$*" >> $SUMMARY
}

mach_to_arch () {
    local ARCH=$1
    case $ARCH in
        amd64)
            MACH=x86_64;;
        i386)
            MACH=i386;;
        arm64)
            MACH=aarch64;;
    esac
    echo $MACH
}

log_firmware_variables () {
    local FWFILE=$1

    WANTED_VARS="SecureBootEnable SbatLevel SHIM_VERBOSE"

    log "Checking firmware variables:"
    for VAR in $WANTED_VARS; do
        log "   $VAR:"
        virt-fw-dump -i "$FWFILE" | grep -A1 "${VAR}.*\(ok\)"
        virt-fw-dump -i "$FWFILE" | grep -A1 "${VAR}.*\(ok\)" >> $LOGFILE
        if [ $? -ne 0 ]; then
            log "      UNSET"
        fi
    done
}

set_sbat_revocations () {
    local FWFILE=$1
    local REVFILE=$2

    if [ "$REVFILE"x = ""x ] || [ "$REVFILE" = "NONE" ]; then
	log "Leaving SbatLevel alone"
	return
    fi

    log "Setting the SbatLevel variable using $REVFILE, contents: "
    cat "$TESTS_DIR/$REVFILE"
    cat "$TESTS_DIR/$REVFILE" >> $LOGFILE
    virt-fw-vars --set-sbat-level "$TESTS_DIR/$REVFILE" -l DEBUG -i "$FWFILE" --output "$FWFILE".new
    mv -f "$FWFILE".new "$FWFILE"
}

run_tests () {
    local DIST=$1
    local ARCH=$2
    local TESTTMP=$3
    local DRYRUN=$4
    local TESTONLY="$5"
    local EFI=$(arch_to_efi $ARCH)

    local TESTS_RUN=0
    local PASS=0
    local FAIL=0
    local INFRAFAIL=0
    local ARCHSKIP=0

    if [ $DRYRUN = 0 ]; then
	cd ~/test/$DIST-$ARCH
	check_error $? "Failed to cd to image test dir ~/test/$DIST-$ARCH"
    fi

    SERIAL=$ARCH-$DIST-serial.txt
    VM_CONF=$ARCH-$DIST-vm.conf
    LOGFILE=$ARCH-$DIST-test.log
    SUMMARY=$ARCH-$DIST-summary.log
    if [ $DRYRUN = 0 ]; then
	rm -f $LOGFILE $SUMMARY
    fi

    DATE=$(date -u)
    log_and_summary "######################"
    log_and_summary "$DATE: TESTS STARTING from $TESTS_DIR"
    log_and_summary "Hostname: $HOSTNAME"
    log_and_summary "Arch:     $ARCH"
    log_and_summary "Hash:     $HASH"

    if [ "$TESTONLY"x = ""x ]; then
	# Run all tests
	TESTS="$TESTS_DIR/*.yaml"
	log_and_summary "TESTS STARTING, running all tests"
    else
	# Run all tests
	TESTS="$TESTS_DIR/$TESTONLY"
	log_and_summary "TESTS STARTING, running only a subset"
    fi
    for TEST in $TESTS; do
	log "  $TEST"
    done

    log_and_summary "######################"
    log_and_summary ""

    for TEST in $TESTS; do
	TESTNAME=$($DT --test $TEST --key name)
	SHIM_SIG=$($DT --test $TEST --key shim_binary)
	SBAT_LEVEL=$($DT --test $TEST --key sbat_level)
	FWKEY=$($DT --test $TEST --key sb_keys)
	EXP_RESULT=$($DT --test $TEST --key expected_result)
	EXTRAFILES=$($DT --test $TEST --key extra_binaries)
	FATAL=$($DT --test $TEST --key fail_behaviour)
	EXTRA_CMDS=$($DT --test $TEST --key special_commands)
	TEST_ARCH=$($DT --test $TEST --key architecture)
	GRUB_BINARY=$($DT --test $TEST --key grub_binary)

	DATE=$(date -u)
	log ""
	log "######################"
	log "Running test  $TEST"
	log " NAME:        $TESTNAME"
	log " HASH:        $HASH"
	log " SHIM_SIG:    $SHIM_SIG"
	log " SBAT_LEVEL:  $SBAT_LEVEL"
	log " FWKEY:       $FWKEY"
	log " EXP_RESULT:  $EXP_RESULT"
	log " EXTRAFILES:  $EXTRAFILES"
	log " FATAL:       $FATAL"
	log " EXTRA_CMDS:  $EXTRA_CMDS"
	log " TEST_ARCH:   $TEST_ARCH"
	log " GRUB_BINARY: $GRUB_BINARY"
	log " DATE:        $DATE"
	log "######################"

	TESTS_RUN=$(($TESTS_RUN + 1))

	case $FWKEY in
	    SB_OFF*)
		SB_GREP="secureboot: Secure boot (disabled|could not be determined)";;
	    *)
		SB_GREP="secureboot: Secure boot enabled";;
	esac

	if [ "$TEST_ARCH"x != ""x ]; then
	    if [ "$TEST_ARCH" != "any" ] && [ "$TEST_ARCH" != "$ARCH" ]; then
		log_and_summary "[ARCH] $TESTNAME"
		log "Skipping test - wants arch $TEST_ARCH, we're $ARCH"
		ARCHSKIP=$(($ARCHSKIP + 1))
		continue
	    fi
	fi

	if [ $DRYRUN = 1 ]; then
	    continue
	fi

	# Copy our test binary into the ESP of the test OS image
	SHIM_BINARY="shim${EFI}.efi.signed-$SHIM_SIG"
	log "  copying $SHIM_BINARY to EFI/debian/shim${EFI}.efi"
	log "install_efi_binaries -a $ARCH -d $DIST \\ "
	log "		     -f \"$TESTTMP/shim${EFI}.efi.signed-$SHIM_SIG\" \\ "
	log "		     -o EFI/debian/shim${EFI}.efi "
	install_efi_binaries -a $ARCH -d $DIST \
			     -f "$TESTTMP/shim${EFI}.efi.signed-$SHIM_SIG" \
			     -o EFI/debian/shim${EFI}.efi
	error=$?
	if [ $error -ne 0 ]; then
	    log "  install_efi_binaries (shim) failed, error $error"
	    INFRAFAIL=$((INFRAFAIL + 1))
	    summary "$TESTNAME : INFRAFAIL"
	    RESULT=INFRAFAIL
	    log "Expected result $EXP_RESULT, got $RESULT"
	    continue
	fi
	# If wanted, copy a specific grub binary into the ESP of the
	# test OS image. Substitute strings in a limited way.
	if [ "$GRUB_BINARY"x != ""x ]; then
	    GRUB_BINARY=$(echo "$GRUB_BINARY" | sed "s/%DEB_ARCH%/$ARCH/g")
	    GRUB_BINARY=$(echo "$GRUB_BINARY" | sed "s/%EFI_ARCH%/$EFI/g")
	    GRUB_BINARY=$(echo "$GRUB_BINARY" | sed "s/%DIST%/$DIST/g")
	    log "  copying $GRUB_BINARY to EFI/debian/grub${EFI}.efi"
	    log "install_efi_binaries -a $ARCH -d $DIST \\ "
	    log "		 -f \"$TESTTMP/grub-signed/$GRUB_BINARY\" \\ "
	    log "		 -o EFI/debian/grub${EFI}.efi "
	    install_efi_binaries -a $ARCH -d $DIST \
				 -f "$TESTTMP/grub-signed/$GRUB_BINARY" \
				 -o EFI/debian/grub${EFI}.efi
	    error=$?
	    if [ $error -ne 0 ]; then
		log "  install_efi_binaries (grub) failed, error $error"
		INFRAFAIL=$((INFRAFAIL + 1))
		summary "$TESTNAME : INFRAFAIL"
		RESULT=INFRAFAIL
		log "Expected result $EXP_RESULT, got $RESULT"
		continue
	    fi
	fi
	rm -f $SERIAL

	MACH=$(mach_to_arch $ARCH)
	cp -f "$MACH-storage.fd.$FWKEY" vars-storage.fd
	set_sbat_revocations vars-storage.fd "$SBAT_LEVEL"
	log_firmware_variables vars-storage.fd

	log "efitest -a $ARCH -d $DIST -s vars-storage.fd -o $VM_CONF"
	efitest -a $ARCH -d $DIST -s vars-storage.fd -o $VM_CONF
	error=$?
	if [ $error -ne 0 ]; then
	    log "  efitest failed, error $error"
	    INFRAFAIL=$((INFRAFAIL + 1))
	    summary "$TESTNAME : INFRAFAIL"
	    RESULT=INFRAFAIL
	    log "Expected result $EXP_RESULT, got $RESULT"
	    continue
	fi
	CURRENT=$(date +%s)
	START=$CURRENT
	TAKEN=0
	RESULT=""
	while [ $TAKEN -lt $BOOT_TIMEOUT ]; do	    
	    TAKEN=$(($CURRENT - $START))
	    # Check we've booted to userland ok
	    if grep -q -E "Debian GNU/Linux.*tty" $SERIAL; then
		# *Also* check that we booted in the right state!
		if grep -q -E "$SB_GREP" $SERIAL; then
		    RESULT=BOOTSUCCESS
		else
		    SB_STATE=$(grep -E "secureboot: Secure boot" $SERIAL)
		    log "SB state mismatch!"
		    log "Expected to find $SB_GREP"
		    log "Got $SB_STATE"
		    INFRAFAIL=$((INFRAFAIL + 1))
		    summary "$TESTNAME : INFRAFAIL"
		    RESULT=INFRAFAIL
		fi
		# Check a trivial command
		run_vm_command $VM_CONF "false"
		# Show SBAT state
		run_vm_command $VM_CONF "mokutil --list-sbat-revocations"
		run_vm_command $VM_CONF "mokutil --set-sbat-policy delete"
		shutdown_vm $VM_CONF ssh $EXTRA_CMDS
		error=$?
		if [ $error != 0 ]; then
		    INFRAFAIL=$((INFRAFAIL + 1))
		    summary "$TESTNAME : INFRAFAIL"
		    RESULT=INFRAFAIL
		fi
		break
	    fi
	    BOOT_FAIL1="failed to load.*Access Denied"
	    BOOT_FAIL2="error: cannot load image"
	    BOOT_FAIL3="to skip.*startup.nsh"
	    BOOT_FAIL4="you need to load the kernel first"
	    BOOT_FAIL5="Security Violation"
	    if grep -q \
		    -e "$BOOT_FAIL1" \
		    -e "$BOOT_FAIL2" \
		    -e "$BOOT_FAIL3" \
		    -e "$BOOT_FAIL4" \
		    -e "$BOOT_FAIL5" \
		    $SERIAL; then
		RESULT=BOOTFAIL
		shutdown_vm $VM_CONF kill
		break
	    fi
	    sleep 1
	    CURRENT=$(date +%s)
	done
	TAKEN=$(($CURRENT - $START))
	if [ "$RESULT"x = ""x ]; then
	    RESULT=BOOTTIMEOUT
	    shutdown_vm $VM_CONF kill
	fi
	log "Expected result $EXP_RESULT, got $RESULT after $TAKEN seconds"
	echo "Serial log follows:" >> $LOGFILE
	echo "######################################" >> $LOGFILE
	cat $SERIAL >> $LOGFILE
	echo >> $LOGFILE
	echo "######################################" >> $LOGFILE
	if [ "$RESULT" = "$EXP_RESULT" ]; then
	    PASS=$(($PASS + 1))
	    log_and_summary "[PASS] $TESTNAME"
	else
	    if [ "$FATAL"x != "FAILOK"x ]; then
		log "Failure here is fatal, stopping all tests"
		echo "$0 $HASH $DIST $ARCH FATAL TEST FAILURE" | \
		    mail -s "testing ABORTED on $HOSTNAME" $MAILTO
		break
	    fi
	    FAIL=$(($FAIL + 1))
	    log_and_summary "[FAIL] $TESTNAME"
	fi
    done

    DATE=$(date -u)
    log_and_summary ""
    log_and_summary "######################"
    log_and_summary "TEST RESULT SUMMARY"
    log_and_summary " PASS:      $PASS"
    log_and_summary " FAIL:      $FAIL"
    log_and_summary " INFRAFAIL: $INFRAFAIL"
    log_and_summary " ARCHSKIP:  $ARCHSKIP"
    log_and_summary " TOTAL;     $TESTS_RUN"
    log_and_summary "######################"
    log_and_summary "$DATE: Test run finished"
}

DRYRUN=0
TESTONLY=""

while getopts ":a:d:h:nt:" o; do
    case "${o}" in
        a)
            ARCHES=${OPTARG}
            ;;
        d)
            DISTS=${OPTARG}
            ;;
        h)
            HASH=${OPTARG}
            ;;
        n)
            DRYRUN=1
            ;;
        t)
            TESTONLY=${OPTARG}
            ;;
        *)
            echo "Unknown option ${o}"
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

[ "$HASH"x != ""x ] || check_error 1 "Need to specify hash"
[ "$DISTS"x != ""x ] || check_error 1 "Need to specify distribution(s)"
[ "$ARCHES"x != ""x ] || check_error 1 "Need to specify architecture(S)"

cleanup () {
    if [ "$TESTTMPS"x != ""x ]; then
	echo "Cleaning up temp test dir(s) $TESTTMPS"
	rm -rf $TESTTMPS
    fi
}
trap "cleanup" EXIT

for ARCH in $(echo "$ARCHES" | tr ',' ' '); do
    for DIST in $(echo "$DISTS" | tr ',' ' '); do

	TESTTMP=$(mktemp -d -p /dev/shm test-shim-$ARCH-$DIST-$HASH-XXXXXXXXXX)
	check_error $? "mktemp failed"
	TESTTMPS="$TESTTMPS $TESTTMP"

	# HACK HACK not unstable!
	if [ $DRYRUN = 0 ]; then
	    rsync -av "$ARTIFACTS/sign/$HASH/$ARCH-$DIST/" "$TESTTMP/"
	    rsync -av "$ARTIFACTS/grub-signed/" "$TESTTMP/grub-signed/"
	fi

	run_tests "$DIST" "$ARCH" "$TESTTMP" "$DRYRUN" "$TESTONLY"

	(echo "$0 $HASH $DIST $ARCH testing completed" ; cat $SUMMARY) | \
            mail -s "testing completed on $HOSTNAME" $MAILTO

	if [ $DRYRUN = 0 ]; then
	    mkdir -p $TESTTMP/$HASH
	    cp $LOGFILE $SUMMARY $TESTTMP/$HASH

	    echo "Copy test log to $ARTIFACTS/test-logs/$HASH/ :"
	    rsync -av "$TESTTMP/$HASH/" "$ARTIFACTS/test-logs/$HASH/"
	fi
    done
done

exit 0
