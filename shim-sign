#!/bin/sh

set -x

# Do all the steps needed to sign the contents of a shim package for a
# given hash, distribution and architecture.

GIT_DIR=shim@jack.einval.org:build/shim.git
ARTIFACTS=shim@jack.einval.org:artifacts
HOSTNAME=$(hostname --fqdn)
MAILTO=steve@einval.com
KEYS="snakeoil wrong NOSIG"

usage () {
    cat <<EOF
$0 <options> - sign a shim build

options:

 -A ARTIFACTS - the directory holding the .deb(s) to sign
 -a ARCH1[,ARCH2,...] - build is for the specified architecture(s)
 -d DIST1[,DIST2,...] - build is for the specified Debian distribution(s)
 -h HASH - the git hash of the build

EOF
}

check_error () {
    if [ $1 -ne 0 ]; then
	echo "$0 $ARTSIN $HASH $DIST $ARCH failed with error $1: $2"
	echo "$0 $ARTSIN $HASH $DIST $ARCH failed with error $1: $2" | \
            mail -s "signing failed on $HOSTNAME" $MAILTO
	exit 1
    fi
}

while getopts ":A:a:d:h:r:" o; do
    case "${o}" in
        A)
            ARTSIN=${OPTARG}
            ;;
        a)
            ARCHES=${OPTARG}
            ;;
        d)
            DISTS=${OPTARG}
            ;;
        h)
            HASH=${OPTARG}
            ;;
        *)
            echo "Unknown option ${o}"
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

[ "$ARTSIN"x != ""x ] || check_error 1 "Need to specify the input dir"
[ "$HASH"x != ""x ] || check_error 1 "Need to specify hash"
[ "$DISTS"x != ""x ] || check_error 1 "Need to specify distribution(s)"
[ "$ARCHES"x != ""x ] || check_error 1 "Need to specify architecture(S)"

cleanup () {
    if [ "$SIGNTMPS"x != ""x ]; then
	echo "Cleaning up tmp dir(s) $SIGNTMPS"
	rm -rf $SIGNTMPS
    fi
}
trap "cleanup" EXIT

for ARCH in $(echo "$ARCHES" | tr ',' ' '); do
    for DIST in $(echo "$DISTS" | tr ',' ' '); do

	echo "Signing $ARTSIN for $ARCH, $DIST, $HASH"

	DEBS=$(find $ARTSIN/ -name 'shim-unsigned*'_$ARCH'.deb')
	NUM_DEBS=$(echo $DEBS | wc -l)
	if [ $NUM_DEBS -gt 1 ]; then
	    check_error 2 "Found too many debs for signing in $ARTSIN: $NUM_DEBS $DEBS"
	elif [ $NUM_DEBS = 0 ]; then
	    check_error 2 "Found no debs for signing in $ARTSIN"
	fi

	# Sign in a temporary dir in /dev/shm for performance and easier cleanup
	SIGNTMP=$(mktemp -d -p /dev/shm sign-shim-$ARCH-$DIST-$HASH-XXXXXXXXXX)
	check_error $? "mktemp failed"
	SIGNTMPS="$SIGNTMPS $SIGNTMP"

	dpkg -x $DEBS $SIGNTMP/extract

	for KEY in $KEYS; do
	    KEYDIR=~/sign/$KEY
	    for file in $SIGNTMP/extract/usr/lib/shim/*.efi; do
		if [ $KEY = "NOSIG" ]; then
		    cp $file $file.signed-NOSIG
		else
		    sbsign  --key $KEYDIR/*.key.nopass --cert $KEYDIR/*.pem --output $file.signed-$KEY $file
		    check_error $? "signature failed for $file using key \"$KEY\""
		fi
	    done

	    ls -al $SIGNTMP/extract/usr/lib/shim

	    mkdir -p "$SIGNTMP/$HASH/$ARCH-$DIST"
	    mv -v "$SIGNTMP/extract/usr/lib/shim/"*signed* "$SIGNTMP/$HASH/$ARCH-$DIST"
	done

	echo "Copy artifacts to $ARTIFACTS/sign/$HASH/ :"
	rsync -av --exclude shim.git "$SIGNTMP/$HASH/" "$ARTIFACTS/sign/$HASH/"
    done
done

exit 0
