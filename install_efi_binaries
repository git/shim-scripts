#!/bin/bash
#
# Place a test binary into an image for EFI boot testing
#

# Set defaults
ARCH=amd64
DIST=buster
FILE=""
OUT=""

usage() {
    echo "$0"
    echo
    echo "Usage: $0 <-a ARCH> <-d DIST> -f <FILE> -o <IMAGE PATH>" 1>&2
    echo
    echo "Copy FILE into the ESP of the efi test image as PATH".
    echo
    echo "-a ARCH       Select architecture; amd64/i386/arm64"
    echo "              (default $ARCH)"
    echo "-d DIST       Select Debian release to use: buster/bullseye/bookworm/unstable"
    echo "              (default $DIST)"
    echo "-f FILE       Select which file should be copied in."
    echo "-o IMAGE PATH Select where the file should be copied to."
    exit 1
}

validate_arg() {
    local VAR=$1
    local VAROPTS="$(echo $2 | tr ':' ' ')"

    for OPT in $VAROPTS; do
        if [ "${!VAR}"x = "$OPT"x ]; then
            return
        fi
    done
    # else
    echo "$VAR ${!VAR} not supported - use one of $VAROPTS"
    exit 1
}

while getopts ":a:d:f:o:" o; do
    case "${o}" in
        a)
            ARCH=${OPTARG}
            ;;
        d)
            DIST=${OPTARG}
            ;;
        f)
            FILE=${OPTARG}
            ;;
        o)
            OUT=${OPTARG}
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

validate_arg ARCH "amd64:i386:arm64"
validate_arg DIST "buster:bullseye:bookworm:unstable"

if [ "$FILE"x = ""x ] || [ "$OUT"x = ""x ]; then
    echo "Missing arg(s)"
    usage
    exit 1
fi

if [ ! -f "$FILE" ]; then
    echo "Input file $FILE does not exist, abort!"
    exit 1
fi
    
TUPLE="$ARCH-$DIST"
DISK=efi-hard-disk-$TUPLE.img

if [ ! -f "$DISK" ]; then
    echo "Disk image $DISK does not exist, abort!"
    exit 1
fi

# OK, we've validated input. Now work out where the ESP is inside the
# disk image. Parse fdisk output!
OFFSET=$(/sbin/fdisk -l $DISK | awk '
      # Find the sector size
      /^Units:/     { gsub("^.* = ","",$0); SECSIZE=$1}
      # Find the start sector of the ESP, which is in column 2
      /EFI System$/ { STARTSEC=$2 }
      END           { print (SECSIZE * STARTSEC) }
')
# If the above fails, we'll get a zero offset
if [ $OFFSET = 0 ]; then
    echo "Disk image $DISK does not seem to include an ESP, abort!"
    exit 1
fi
mcopy -v -o -i ${DISK}@@${OFFSET} "${FILE}" "::${OUT}"

# And exit with the error code from mcp

