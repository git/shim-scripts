#!/bin/sh

# Do all the steps needed to build a shim package for a given hash,
# distribution and architecture. Run from inside a shim git directory.

GIT_DIR=shim@jack.einval.org:build/shim.git
ARTIFACTS=shim@jack.einval.org:artifacts
HOSTNAME=$(hostname --fqdn)
MAILTO=steve@einval.com

usage () {
    cat <<EOF
$0 <options> - build shim

options:

 -a ARCH1[,ARCH2,...] - build for the specified architecture(s)
 -d DIST1[,DIST2,...] - build for the specified Debian distribution(s)
 -h HASH - build from the specified git hash/tag

EOF
}

check_error () {
    if [ $1 -ne 0 ]; then
        echo "$0 $HASH $DIST $ARCH failed with error $1: $2"
        exit 1
    fi
}

while getopts ":a:d:h:r:" o; do
    case "${o}" in
        a)
            ARCHES=${OPTARG}
            ;;
        d)
            DISTS=${OPTARG}
            ;;
        h)
            HASH=${OPTARG}
            ;;
        *)
            echo "Unknown option ${o}"
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

[ "$HASH"x != ""x ] || check_error 1 "Need to specify hash"
[ "$DISTS"x != ""x ] || check_error 1 "Need to specify distribution(s)"
[ "$ARCHES"x != ""x ] || check_error 1 "Need to specify architecture(S)"

cleanup () {
    if [ "$BUILDTMPS"x != ""x ]; then
        echo "Cleaning up build dir(s) $BUILDTMPS"
        rm -rf $BUILDTMPS
    fi
}
trap "cleanup" EXIT

APT_GET_OPTS="-o Dpkg::Options::=--force-confnew \
              -o Dpkg::Options::=--force-confmiss"

for ARCH in $(echo "$ARCHES" | tr ',' ' '); do
    for DIST in $(echo "$DISTS" | tr ',' ' '); do

        echo "Building for $ARCH, $DIST, $HASH"

        # We'll use this a lot
        SCHROOT="shim-$DIST-$ARCH"

        echo "Building from sources in $GIT_DIR"
        echo "Updating packages and build-deps in chroot $SCHROOT"
        echo 'debconf debconf/frontend select Noninteractive' | \
            schroot -u root -c $SCHROOT debconf-set-selections
        check_error $? "debconf-set-selections failed "
        schroot -u root -c $SCHROOT -- apt-get -y \
                $APT_GET_OPTS update --allow-releaseinfo-change
        check_error $? "apt-get update fail"
        schroot -u root -c $SCHROOT -- apt-get -y \
                $APT_GET_OPTS dist-upgrade
        check_error $? "apt-get dist-upgrade fail"
        schroot -u root -c $SCHROOT -- apt-get -y \
                $APT_GET_OPTS install build-essential fakeroot git
        check_error $? "apt-get install fakeroot build-essential fail"
        schroot -u root -c $SCHROOT -- apt-get -y \
                $APT_GET_OPTS build-dep shim
        check_error $? "apt-get build-dep fail"

        # Build in a temporary dir in /dev/shm for performance and easier cleanup
        BUILDTMP=$(mktemp -d -p /dev/shm shim-$ARCH-$DIST-$HASH-XXXXXXXXXX)
        check_error $? "mktemp failed"
        BUILDTMPS="$BUILDTMPS $BUILDTMP"

        SUCCESS=0

        # Loop: create build tree, copy the git tree in, build
        for i in 1 2; do
            echo "Running build #$i"
    
            # Setup
            BUILD="$BUILDTMP/$HASH/$ARCH-$DIST/build-$HOSTNAME-$i"
            mkdir -p "$BUILD"
            check_error $? "mkdir failed for $BUILD"
            rsync -a "$GIT_DIR/" "$BUILD/shim.git/"
            check_error $? "rsync failed for $BUILD"

            cd "$BUILD/shim.git"
            check_error $? "cd to $BUILD/shim.git failed"
            git checkout $HASH
            check_error $? "git checkout $HASH failed"

            schroot -u root -c $SCHROOT -- apt-get -y \
                    $APT_GET_OPTS build-dep .

            # Build, then grab the logs and artifacts
            schroot -d "$BUILD/shim.git" -c $SCHROOT -- \
                    script -e -c "dpkg-buildpackage -b --no-sign" ../build.log 
            error=$?

            if [ $error -eq 0 ]; then
                echo "Build succeeded in $BUILD"
                SUCCESS=$((SUCCESS + 1))
                grep -E '^[[:alnum:]]{64}' "$BUILD/build.log" > "$BUILD/checksums.log"
                mv "$BUILD" "$BUILD-SUCCESS"
            else
                echo "Build failed in $BUILD"
                echo "Build failed in $BUILD" | mail -s "build failed on $HOSTNAME" $MAILTO
                mv "$BUILD" "$BUILD-FAILURE"
            fi
        done

        echo "Copy artifacts and logs to $ARTIFACTS/build/$HASH/:"
        rsync -av --exclude shim.git "$BUILDTMP/$HASH/" "$ARTIFACTS/build/$HASH/"
    done
done

exit 0
